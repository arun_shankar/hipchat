package com.atlassian.hipchattask;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_RX = 0;
    private static final int TYPE_SENT = 1;

    private ArrayList<ChatString> chatStrings;

    public ChatAdapter(ArrayList<ChatString> chatStrings) {
        this.chatStrings = chatStrings;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_RX) {
            //inflate your layout and pass it to view holder
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rx_view, parent, false);
            return new VHReceived(inflatedView);
        } else if (viewType == TYPE_SENT) {
            //inflate your layout and pass it to view holder
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.sent_view, parent, false);
            return new VHSent(inflatedView);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ChatString dataItem = getItem(position);
        if (holder instanceof VHReceived) {
            VHReceived vhReceived = (VHReceived) holder;
            vhReceived.msg.setText(dataItem.getMessage());
        } else if (holder instanceof VHSent) {
            VHSent vhSent = (VHSent) holder;
            vhSent.msg.setText(dataItem.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return chatStrings.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(getItem(position).getType().ordinal() == TYPE_RX)
            return TYPE_RX;

        return TYPE_SENT;
    }

    private ChatString getItem(int position) {
        return chatStrings.get(position);
    }

    class VHReceived extends RecyclerView.ViewHolder {
        private TextView msg;

        public VHReceived(View v) {
            super(v);
            msg = (TextView) v.findViewById(R.id.msg);
        }
    }

    class VHSent extends RecyclerView.ViewHolder {
        private TextView msg;

        public VHSent(View v) {
            super(v);
            msg = (TextView) v.findViewById(R.id.msg);
        }
    }

}

