package com.atlassian.hipchattask;

public class ChatString {

    String message;

    public enum ChatType {
        TYPE_RX,
        TYPE_SENT;
    }
    private ChatType type;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ChatType getType() {
        return type;
    }

    public void setType(ChatType type) {
        this.type = type;
    }
}
