package com.atlassian.hipchattask;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmoticonsParser {

    private static final Pattern emoticonPattern = Pattern.compile("\\(([a-zA-Z0-9]{1,15})\\)",Pattern.DOTALL);

    public static ArrayList<String> parse(String inputText){
        ArrayList<String> emoticons = new ArrayList();
        Matcher matcher = emoticonPattern.matcher(inputText);
        while (matcher.find()){
            String match = matcher.group();
            match = match.replaceAll("\\(","");
            match = match.replaceAll("\\)","");
            emoticons.add(match);
        }
        return emoticons;
    }

}
