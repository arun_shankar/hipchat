package com.atlassian.hipchattask;

import android.content.Context;
import android.os.Looper;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Observable;
import rx.Subscriber;

public class LinksParser {

    public static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static Observable<Link> parse(String text, Context context) {
        Matcher matcher = urlPattern.matcher(text);
        while(matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            String urlStr = text.substring(matchStart, matchEnd);
            return getTitle(context,urlStr);
        }
        return null;
    }

    private static Observable<Link> getTitle(Context context, String text){
        return Observable.create(new Observable.OnSubscribe<Link>() {
            @Override
            public void call(Subscriber<? super Link> subscriber) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        ((MainActivity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                WebView webView = new WebView(context);
                                webView.loadUrl(text);
                                webView.setWebViewClient(new WebViewClient(){
                                    @Override
                                    public void onPageFinished(WebView view, String url) {
                                        super.onPageFinished(view, url);
                                        Link link = new Link();
                                        link.setUrl(text);
                                        link.setTitle(view.getTitle());
                                        subscriber.onNext(link);
                                        subscriber.onCompleted();
                                    }
                                });
                            }
                        });
                        Looper.loop();
                    }
                }).start();
            }
        });
    }

    public static boolean containsUrl(String text) {
        Matcher matcher = urlPattern.matcher(text);
        return matcher.find();
    }

}
