package com.atlassian.hipchattask;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<ChatString> chatStrings;
    private ChatAdapter mAdapter;
    private EditText chatBox;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        chatBox = (EditText) findViewById(R.id.chatBox);
        chatBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0)
                    sendButton.setEnabled(true);
                else
                    sendButton.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(v -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(chatBox.getWindowToken(), 0);
            ChatString sentString = new ChatString();
            String inputText = chatBox.getText().toString().trim();
            sentString.setMessage(inputText);
            sentString.setType(ChatString.ChatType.TYPE_SENT);
            chatStrings.add(sentString);
            mAdapter.notifyDataSetChanged();
            chatBox.setText("");
            if (LinksParser.containsUrl(inputText)) {
                MessageParser.parseLink(inputText, MainActivity.this)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(link -> {
                            ArrayList<Link> links = new ArrayList<>();
                            links.add(link);
                            Message message = new Message();
                            message.setLinks(links);
                            message.setEmoticons(EmoticonsParser.parse(inputText));
                            message.setMentions(MentionsParser.parse(inputText));
                            ChatString rxString = new ChatString();
                            rxString.setMessage(message.toString());
                            rxString.setType(ChatString.ChatType.TYPE_RX);
                            chatStrings.add(rxString);
                            mAdapter.notifyDataSetChanged();
                        }, Throwable::printStackTrace);
            } else {
                Message message = MessageParser.parse(inputText);
                ChatString rxString = new ChatString();
                rxString.setMessage(message.toString());
                rxString.setType(ChatString.ChatType.TYPE_RX);
                chatStrings.add(rxString);
                mAdapter.notifyDataSetChanged();
            }
        });

        chatStrings = new ArrayList<>();
        mAdapter = new ChatAdapter(chatStrings);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }
}
