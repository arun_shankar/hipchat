package com.atlassian.hipchattask;

import java.util.ArrayList;

public class MentionsParser {

    private static final String MENTIONS_REGEX = "^[a-zA-Z0-9]+$";

    public static ArrayList<String> parse(String inputText){
        ArrayList<String> mentions = new ArrayList();
        String[] strings = inputText.split("@");
        for(int i=1;i<strings.length;i++){
            String s = strings[i];
            s = s.trim().split(" ")[0];
            if(s.matches(MENTIONS_REGEX))
                mentions.add(s);
        }
        return mentions;
    }


}
