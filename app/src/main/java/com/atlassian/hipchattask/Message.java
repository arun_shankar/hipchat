package com.atlassian.hipchattask;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Message {

    ArrayList<Link> links;
    ArrayList<String> emoticons;
    ArrayList<String> mentions;

    public ArrayList<Link> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<Link> links) {
        this.links = links;
    }

    public ArrayList<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(ArrayList<String> emoticons) {
        this.emoticons = emoticons;
    }

    public ArrayList<String> getMentions() {
        return mentions;
    }

    public void setMentions(ArrayList<String> mentions) {
        this.mentions = mentions;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }


}
