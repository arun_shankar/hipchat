package com.atlassian.hipchattask;

import android.content.Context;

import rx.Observable;

public class MessageParser {

    public static Observable<Link> parseLink(String inputText, Context context) {
        return LinksParser.parse(inputText, context);
    }

    public static Message parse(String inputText) {
        Message message = new Message();
        message.setEmoticons(EmoticonsParser.parse(inputText));
        message.setMentions(MentionsParser.parse(inputText));
        return message;
    }

}
